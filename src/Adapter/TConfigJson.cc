#include "RCParser/Adapter/TConfigJson.h"

#ifdef JSON_FOUND
#include <nlohmann/json.hpp>

TConfigNode *TConfigJson::Parse(const char *contents, int size)
{
    UNUSED(size);
    
    nlohmann::json j;
    if (contents != NULL && contents[0] == '\0') j = nlohmann::json("{}");
    else {

        try { j = nlohmann::json::parse(contents); }
        catch(...) {
            
            std::cerr << "Failed to parse: `" << gPrint->MakeItShorter(contents, 25) << "`"; 
            j = nlohmann::json("{}");
        };
    }

    std::function<TConfigNode*(const char *, const nlohmann::json&)> _callback = [&_callback](const char *tag, const nlohmann::json& j) mutable {

        TConfigNode *node = new TConfigNode(tag);
                     node->SetIndexer(TConfigNode::Indexer::Dictionary);

        for (auto& element : j.items()) {

            TString nodeName = element.key();
            if(nodeName.EqualTo("")) continue;

            TConfigNode *childNode = _callback(nodeName.Data(), element.value());
            if(element.value().is_array()) childNode->SetIndexer(TConfigNode::Indexer::Array);
            else childNode->SetIndexer(TConfigNode::Indexer::Dictionary);

            if(element.value().is_null()) childNode->SetContent(nullptr);
            else if(element.value().is_boolean()) childNode->SetContent((bool) element.value());
            else if(element.value().is_number_integer()) childNode->SetContent((int) element.value());
            else if(element.value().is_number_float()) childNode->SetContent((float) element.value());
            else if(element.value().is_number()) childNode->SetContent((double) element.value());
            else if(element.value().is_string()) childNode->SetContent(TString((std::string) element.value()));
            else if(element.value().is_binary()) childNode->SetContent((bool) element.value());

            node->Attach(*childNode);
        }

        return node;
    };

    return _callback("root", j);
}

void TConfigJson::Save(const char *filename, const TConfigNode& root) {

    nlohmann::json jRoot;

    // Lambda function to convert TConfigNode to JSON recursively
    std::function<void(const TConfigNode&, nlohmann::json&)> _callback_write = [&](const TConfigNode& node, nlohmann::json& jNode) {
        
        for (const auto& element : node.GetElements()) {
        
            if (element->GetType() == TConfigNode::Type::Empty) {

                nlohmann::json childNode;
                if(element->IsArray()) childNode = nlohmann::json::array();

                _callback_write(*element, childNode);
                if(jNode.is_array()) jNode.push_back(childNode);
                else jNode[element->GetTag().Data()] = childNode;

            } else {

                // Handle variant content types
                const auto& content = element->GetContent();

                std::visit([&](const auto& value) {

                    // Handle different types here
                    if constexpr (std::is_same_v<std::decay_t<decltype(value)>, std::nullptr_t>) {
                        if(jNode.is_array()) jNode.push_back(nullptr);
                        else jNode[element->GetTag().Data()] = nullptr;
                    } else if constexpr (std::is_same_v<std::decay_t<decltype(value)>, TString>) {
                        if(jNode.is_array()) jNode.push_back(std::string(value));
                        else jNode[element->GetTag().Data()] = std::string(value);
                    } else if constexpr (std::is_same_v<std::decay_t<decltype(value)>, int>) {
                        if(jNode.is_array()) jNode.push_back(value);
                        else jNode[element->GetTag().Data()] = value;
                    } else if constexpr (std::is_same_v<std::decay_t<decltype(value)>, double>) {
                        if(jNode.is_array()) jNode.push_back(value);
                        else jNode[element->GetTag().Data()] = value;
                    } else if constexpr (std::is_same_v<std::decay_t<decltype(value)>, float>) {
                        if(jNode.is_array()) jNode.push_back(value);
                        else jNode[element->GetTag().Data()] = value;
                    } else if constexpr (std::is_same_v<std::decay_t<decltype(value)>, bool>) {
                        if(jNode.is_array()) jNode.push_back(value);
                        else jNode[element->GetTag().Data()] = value;
                    }
                }, content);

            }
        }
    };

    // Convert the root TConfigNode to JSON
    _callback_write(root, jRoot);

    // Write JSON to file
    std::ofstream outFile(filename);
    if (outFile.is_open()) {
        outFile << jRoot.dump(4); // Pretty printing with an indent of 4 spaces
        outFile.close();
    } else {
        std::cerr << "Failed to open file for writing: " << filename << std::endl;
    }
}

bool TConfigJson::Supports(const char *filename) {
    return TString(filename).EndsWith(".json");
}

#endif