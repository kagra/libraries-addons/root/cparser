#include "RCParser/Adapter/TConfigYaml.h"

#ifdef YAML_FOUND

#include <yaml-cpp/yaml.h>

TConfigNode *TConfigYaml::Parse(const char *contents, int size)
{
    UNUSED(size);
    
    YAML::Node y;
    
    try { y = YAML::Load(contents); }
    catch(...) {
        
        std::cerr << "Failed to parse: `" << gPrint->MakeItShorter(contents, 25) << "`"; 
    };

    std::function<TConfigNode*(const char *, const YAML::Node&)> _callback = [&_callback](const char *tag, const YAML::Node& y) mutable {

        TConfigNode *node = new TConfigNode(tag);

        if (y.IsMap()) {
            node->SetIndexer(TConfigNode::Indexer::Dictionary);
            for (const auto& element : y) {
                TConfigNode* child = _callback(element.first.Scalar().c_str(), element.second);
                node->Attach(*child);
            }
        } else if (y.IsSequence()) {
            node->SetIndexer(TConfigNode::Indexer::Array);
            for (size_t i = 0; i < y.size(); ++i) {
                TConfigNode* child = _callback(TString::Itoa(i,10).Data(), y[i]);
                node->Attach(*child);
            }
        } else if (y.IsScalar()) {
            node->SetContent(y.Scalar().c_str());
        }
        return node;
    };

    return _callback("root", y);
}

void TConfigYaml::Save(const char *filename, const TConfigNode& root) {

    std::ofstream outFile(filename);
    if (!outFile.is_open()) {
        std::cerr << "Unable to open file for writing: " << filename << "\n";
        return;
    }

    YAML::Emitter out;
    out << YAML::BeginMap;

    std::function<void(const TConfigNode&, YAML::Emitter&)> _callback_writer = [&](const TConfigNode& node, YAML::Emitter& out) {

        for (const auto& childNode : node.GetElements()) {

            if (childNode->GetType() == TConfigNode::Type::Empty) {

                if (childNode->IsArray()) {

                    out << YAML::Key << childNode->GetTag();
                    out << YAML::BeginSeq;
                    _callback_writer(*childNode, out); // Recursive call
                    out << YAML::EndSeq;

                } else if (childNode->IsDictionary()) {

                    out << YAML::Key << childNode->GetTag();
                    out << YAML::BeginMap;
                    _callback_writer(*childNode, out); // Recursive call
                    out << YAML::EndMap;
                } 

            } else {
                
                if(node.IsDictionary() && childNode->GetType() != TConfigNode::Type::Null)
                    out << YAML::Key << childNode->GetTag().Data();
            
                TConfigNode::Value content = childNode->GetContent();
                if (auto str = std::get_if<TString>(&content)) {
                    out << YAML::Value << str->Data();
                } else if (auto i = std::get_if<int>(&content)) {
                    out << YAML::Value << *i;
                } else if (auto i = std::get_if<int>(&content)) {
                    out << YAML::Value << *i;
                } else if (auto f = std::get_if<float>(&content)) {
                    out << YAML::Value << *f;
                } else if (auto d = std::get_if<double>(&content)) {
                    out << YAML::Value << *d;
                } else if (auto b = std::get_if<bool>(&content)) {
                    out << YAML::Value << (*b ? "true" : "false");
                }
            }
        }
    };

    _callback_writer(root, out); // Start serialization with the root node
    outFile << out.c_str();
}

bool TConfigYaml::Supports(const char *filename) {
    return TString(filename).EndsWith(".yaml");
}

#endif