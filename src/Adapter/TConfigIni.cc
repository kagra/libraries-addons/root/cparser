#include "RCParser/Adapter/TConfigIni.h"
#ifdef INI_FOUND
  #include "RCParser/Impl/INIReader.h"

/**
 * @brief Parse the contents into a TConfigNode
 * @param contents The content to be parsed
 * @param size The size of the content
 * @return Parsed TConfigNode
 * @copydoc TConfigAdapter::Parse
 */

TConfigNode *TConfigIni::Parse(const char *contents, int size) {

    INIReader reader(contents, size);

    TConfigNode* root = new TConfigNode("root");
                 root->SetIndexer(TConfigNode::Indexer::Dictionary);

    if(reader.ParseError()) {
    
        std::cerr << "Failed to parse: `" << gPrint->MakeItShorter(contents, 25) << "`"; 
        return root;
    }

    // Assume sections and keys are represented as nodes
    for (const auto& section : reader.GetSections()) {

        TConfigNode* sectionNode = new TConfigNode(section.c_str());
        sectionNode->SetIndexer(TConfigNode::Indexer::Dictionary);

        for (const auto& name : reader.GetFields(section)) {
            TString value = reader.Get(section, name, "UNKNOWN");
            TConfigNode* keyNode = new TConfigNode(name.c_str());
            keyNode->SetContent(value); // Assuming SetContent can take TString

            sectionNode->Attach(*keyNode);
        }

        root->Attach(*sectionNode);
    }

    return root;
}

void TConfigIni::Save(const char *filename, const TConfigNode& root) {

    std::ofstream outFile(filename);
    if (!outFile.is_open()) {
        std::cerr << "Failed to open file for writing: " << filename << std::endl;
        return;
    }

    std::function<void(const TConfigNode&, std::ofstream&)> _writeSection = [&](const TConfigNode& node, std::ofstream& outFile) {
        for (const auto& element : node.GetElements()) {
            // Assuming sections are first-level children and keys are second-level
            if (element->IsDictionary()) {
                outFile << "[" << element->GetTag().Data() << "]" << std::endl;
                for (const auto& child : element->GetElements()) {
                    // Assuming child node's content can be converted to string
                    outFile << child->GetTag().Data() << " = " << TString(*child) << std::endl;
                }
                outFile << std::endl; // Add a blank line after each section
            }
        }
    };

    _writeSection(root, outFile);
    outFile.close();
}

bool TConfigIni::Supports(const char *filename) {
    return TString(filename).EndsWith(".ini");
}

#endif