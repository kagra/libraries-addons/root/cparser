/**
 **********************************************
 *
 * \file TLigoTable.cc
 * \brief Source code of the TLigoTable class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include "RCParser/TLigoTable.h"
ClassImp(TLigoTable)

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

std::vector<TString> TLigoTable::GetColumns()
{
   std::vector<TString> columns;

   for(TableDictIter it = dict.begin(); it != dict.end(); it++)
      columns.push_back(it->first);

   return columns;
}

TString TLigoTable::Get(TString key, int index)
{
   key = ROOT::IOPlus::Helpers::Explode(",", key)[0];

   TableIter it = map.find(key+","+TString::Itoa(index,10));
   if(it == map.end()) return "";

   return it->second;
}

void TLigoTable::SetType(TString key, TLigoTable::Type type)
{
   key = ROOT::IOPlus::Helpers::Explode(",", key)[0];
   dict.insert({key, type});
}

void TLigoTable::Print(Option_t *opt)
{
   TString option = opt;
           option.ToLower();

   std::vector<TString> columnNames = this->GetColumns();
   
   int nEntries = 0;
   for(int i = 0, N = columnNames.size(); i < N; i++) 
      nEntries = TMath::Max(nEntries, this->GetEntries(columnNames[i]));

   std::cout << "OBJ: " << this->Class()->GetName() << "  " << this->GetName() << "  #entries = " << nEntries << "  #cols = " << columnNames.size() << std::endl;

   if(gDebug > 2 || option.Contains("all")) {

      for(int i = 0, I = this->GetEntries(); i < I; i++) {   
            std::cout << "Entry #" << (i+1) << " (";             
            for(int j = 0, J = columnNames.size(); j < J; j++) {                
               
               if(j > 0) std::cout << ", ";
               std::cout << columnNames[j] << ": `"<< gPrint->MakeItShorter(this->Get(columnNames[j], i)) << "`";
            }
            std::cout << ")" << std::endl;
      }
   }
}

TLigoTable::Type TLigoTable::GetType(TString key)
{
   key = ROOT::IOPlus::Helpers::Explode(",", key)[0];
   TableDictIter it = dict.find(key);
   
   return it == dict.end() ? Type::undefined : it->second;
}

void TLigoTable::AddColumn(TString key, TLigoTable::Type type)
{
   key = ROOT::IOPlus::Helpers::Explode(",", key)[0];
   dict.insert({key, type});
   map.insert({key, "0"});
}

int TLigoTable::GetEntries(TString key)
{
   key = ROOT::IOPlus::Helpers::Explode(",", key)[0];

   TableIter it = map.find(key);
   return it == map.end() ? 0 : it->second.Atoi();
}

int TLigoTable::GetEntries()
{
   std::vector<TString> columnNames = this->GetColumns();
   
   int nEntries = 0;
   for(int i = 0, N = columnNames.size(); i < N; i++) 
      nEntries = TMath::Max(nEntries, this->GetEntries(columnNames[i]));

   return nEntries;
}

void TLigoTable::Add(TString key, TString value)
{
   if(!this->HasColumn(key)) this->AddColumn(key, Type::lstring);

   key = ROOT::IOPlus::Helpers::Explode(",", key)[0];
   int nEntries = this->GetEntries(key);

   value = value.Strip(TString::kBoth, '"');

   map[key] = TString::Itoa(nEntries+1,10);
   map[key+","+TString::Itoa(nEntries,10)] = value;
}

void TLigoTable::RemoveColumn(TString key)
{
   key = ROOT::IOPlus::Helpers::Explode(",", key)[0];
   dict.erase(key);

   int nEntries = this->GetEntries(key);

   map.erase(key);
   for(int i = 0; i < nEntries; i++)
      map.erase(key+","+TString::Itoa(i,10));
}

bool TLigoTable::HasColumn(TString key)
{
   key = ROOT::IOPlus::Helpers::Explode(",", key)[0];
   return dict.find(key) == dict.end();
}

bool TLigoTable::Has(TString key, int index)
{
   key = ROOT::IOPlus::Helpers::Explode(",", key)[0];
   return map.find(key+","+TString::Itoa(index,10)) == map.end();
}
