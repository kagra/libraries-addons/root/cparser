#include "RCParser/TConfigAdapter.h"

TConfigNode *TConfigAdapter::Load(const char *fName)
{
    struct stat stat_buf;
    if (stat(fName, &stat_buf) == 0) {

        std::ifstream f(fName);
        if(f.is_open()) {

            std::string buffer((std::istreambuf_iterator<char>(f)), std::istreambuf_iterator<char>());
            return this->Parse(buffer.c_str(), buffer.size());
        }
    }

    return nullptr;
}