/**
 **********************************************
 *
 * \file TLigoFile.cc
 * \brief Source code of the TLigoFile class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include "TLigoFile.h"
#include "RCParser/Impl/LALSuite.h"

ClassImp(TLigoFile)

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

TLigoFile::TLigoNode TLigoFile::ParseXMLNode(TXMLNode *node)
{
   TLigoNode _ptr = std::make_pair(TLigoFile::PTR_NODE::Empty, (void *) NULL);
   if(node == nullptr) return _ptr;

   bool isElement = !TString(node->GetNodeName()).EqualTo("text");
   if  (isElement) {

      ParseXMLNode(node, _ptr);
      if(_ptr.second == NULL) return _ptr;

      ParseXMLAttributes(node, _ptr);
      ParseXMLElements(node, _ptr);
   }
   
   return _ptr;
}

void TLigoFile::ParseXMLNode(TXMLNode *node, TLigoNode &pair)
{
   TLigoFile::PTR_NODE dataClass = str2node(node->GetNodeName());
   void *_ptr = NULL;

   switch(dataClass)
   {
      case PTR_NODE::Empty    : _ptr = NULL;
         break;
      case PTR_NODE::Column    : _ptr = (void *) new Column;
         break;
      case PTR_NODE::LIGO_LW    : _ptr = (void *) new LIGO_LW;
         break;
      case PTR_NODE::PCDATA     : _ptr = (void *) new PCDATA;
         break;
      case PTR_NODE::CDATA      : _ptr = (void *) new CDATA;
         break;
      case PTR_NODE::Table      : _ptr = (void *) new Table;
         break;
      case PTR_NODE::Array      : _ptr = (void *) new Array;
         break;
      case PTR_NODE::IGWDFrame  : _ptr = (void *) new IGWDFrame;
         break;
      case PTR_NODE::AdcData    : _ptr = (void *) new AdcData;
         break;
      case PTR_NODE::AdcInterval: _ptr = (void *) new AdcInterval;
         break;
      case PTR_NODE::Detector   : _ptr = (void *) new Detector;
         break;
      case PTR_NODE::Comment    : _ptr = (void *) new Comment;
         ((Comment*) _ptr)->_self.Data = ROOT::IOPlus::Helpers::Explode("\n", TString(node->GetText()));
         break;
      case PTR_NODE::Param      : _ptr = (void *) new Param;
         ((Param*)   _ptr)->_self.Data = ROOT::IOPlus::Helpers::Explode("\n", TString(node->GetText()));
         break;
      case PTR_NODE::Dim        : _ptr = (void *) new Dim;
         ((Dim*)     _ptr)->_self.Data = ROOT::IOPlus::Helpers::Explode("\n", TString(node->GetText()));
         break;
      case PTR_NODE::Stream     : _ptr = (void *) new Stream;
         ((Stream*)  _ptr)->_self.Data = ROOT::IOPlus::Helpers::Explode("\n", TString(node->GetText()));
         break;
      case PTR_NODE::Time       : _ptr = (void *) new Time;
         ((Time*)    _ptr)->_self.Data = ROOT::IOPlus::Helpers::Explode("\n", TString(node->GetText()));
         break;
   }
   
   pair.first = str2node(node->GetNodeName());
   pair.second = _ptr;
}

TLigoFile::TLigoAttributes TLigoFile::ParseXMLAttributes(TXMLNode *node)
{
   TLigoAttributes attributes;
   
   TList *attrList = node->GetAttributes();
   TIter Next(attrList); 

   TXMLAttr *attr = nullptr;
   while ( ( attr = (TXMLAttr*) Next()) ) {
      attributes.insert({attr->GetName(), attr->GetValue()});
   }

   return attributes;
}

void TLigoFile::ParseXMLAttributes(TXMLNode *node, TLigoNode &ptr)
{
   if(node == NULL) return;
   
   TLigoAttributes nodeAttributes = ParseXMLAttributes(node);
   PTR_NODE dataClass = ptr.first;
   void *_ptr     = ptr.second;

   switch(dataClass)
   {
      case PTR_NODE::Empty:
         break;
      case PTR_NODE::PCDATA:
         break;
      case PTR_NODE::CDATA:
         break;

      case PTR_NODE::LIGO_LW:
         ((LIGO_LW *) _ptr)->Name.Data = nodeAttributes["Name"];
         ((LIGO_LW *) _ptr)->Type.Data = nodeAttributes["Type"]; 
         break;

      case PTR_NODE::Column:
         ((Column *) _ptr)->Name.Data     = nodeAttributes["Name"];
         ((Column *) _ptr)->Type.Data     = nodeAttributes["Type"];
         ((Column *) _ptr)->Unit.Data     = nodeAttributes["Unit"];
         break;

      case PTR_NODE::Param:
         ((Param *) _ptr)->Name.Data     = nodeAttributes["Name"];
         ((Param *) _ptr)->Type.Data     = nodeAttributes["Type"];
         ((Param *) _ptr)->Start.Data    = nodeAttributes["Start"];
         ((Param *) _ptr)->Scale.Data    = nodeAttributes["Scale"];
         ((Param *) _ptr)->Unit.Data     = nodeAttributes["Unit"];
         ((Param *) _ptr)->DataUnit.Data = nodeAttributes["DataUnit"];
         break;

      case PTR_NODE::Comment:
         break;

      case PTR_NODE::Dim:
         ((Dim *) _ptr)->Name.Data     = nodeAttributes["Name"];
         ((Dim *) _ptr)->Unit.Data     = nodeAttributes["Unit"];
         ((Dim *) _ptr)->Start.Data    = nodeAttributes["Start"];
         ((Dim *) _ptr)->Scale.Data    = nodeAttributes["Scale"];
         break;

      case PTR_NODE::Table:
         ((Param *) _ptr)->Name.Data     = nodeAttributes["Name"];
         ((Param *) _ptr)->Type.Data     = nodeAttributes["Type"];
         break;

      case PTR_NODE::Array:
         ((Array *) _ptr)->Name.Data     = nodeAttributes["Name"];
         ((Array *) _ptr)->Type.Data     = nodeAttributes["Type"];
         ((Array *) _ptr)->Unit.Data     = nodeAttributes["Unit"];
         break;

      case PTR_NODE::Stream:
         ((Stream *) _ptr)->Name.Data      = nodeAttributes["Name"];
         ((Stream *) _ptr)->Type           = str2stream(nodeAttributes["Type"]);
         ((Stream *) _ptr)->Delimiter.Data = nodeAttributes["Delimiter"];
         ((Stream *) _ptr)->Encoding.Data  = nodeAttributes["Encoding"];
         ((Stream *) _ptr)->Content.Data   = nodeAttributes["Content"];
         break;

      case PTR_NODE::IGWDFrame:
         ((IGWDFrame *) _ptr)->Name.Data      = nodeAttributes["Name"];
         break;

      case PTR_NODE::AdcData:
         ((AdcData *) _ptr)->Name.Data      = nodeAttributes["Name"];
         break;

      case PTR_NODE::AdcInterval:
         ((AdcInterval *) _ptr)->Name.Data      = nodeAttributes["Name"];
         ((AdcInterval *) _ptr)->StartTime.Data = nodeAttributes["StartTime"];
         ((AdcInterval *) _ptr)->DeltaT.Data    = nodeAttributes["DeltaT"];
         break;

      case PTR_NODE::Time:
         ((Time *) _ptr)->Name.Data = nodeAttributes["Name"];
         ((Time *) _ptr)->Type       = str2time(nodeAttributes["Type"]);
         break;

      case PTR_NODE::Detector:
         ((Detector *) _ptr)->Name.Data      = nodeAttributes["Name"];
         break;
   }
}

TLigoFile::TLigoElements TLigoFile::ParseXMLElements(TXMLNode *node)
{
   TLigoElements elements;
   if(node == nullptr) return elements;

   TXMLNode *childNode = node->GetChildren();
   while (childNode != NULL) {

      bool isElement = !TString(childNode->GetNodeName()).EqualTo("text");
      if ( isElement ) elements.push_back(TLigoFile::ParseXMLNode(childNode));  

      childNode = childNode->GetNextNode();
   }
   
   return elements;
}

void TLigoFile::ParseXMLElements(TXMLNode *node, TLigoNode &ptr)
{
   if(node == NULL) return;
   
   PTR_NODE dataClass = ptr.first;
   void *_ptr     = ptr.second;

   TLigoElements elements = ParseXMLElements(node);
   for(int i = 0, N = elements.size(); i < N; i++) {
      
      switch(dataClass)
      {
         case PTR_NODE::Empty:
            break;

         case PTR_NODE::PCDATA:
            break;

         case PTR_NODE::CDATA:
            break;

         case PTR_NODE::LIGO_LW: ((LIGO_LW *) _ptr)->_self.push_back(elements[i]);
            break;

         case PTR_NODE::Param:
            break;

         case PTR_NODE::Column:
            break;

         case PTR_NODE::Comment:
            break;

         case PTR_NODE::Dim:
            break;

         case PTR_NODE::Table: ((Table *) _ptr)->_self.push_back(elements[i]);
            break;

         case PTR_NODE::Array: ((Array *) _ptr)->_self.push_back(elements[i]);
            break;
         case PTR_NODE::Stream:
            break;

         case PTR_NODE::IGWDFrame: ((IGWDFrame *) _ptr)->_self.push_back(elements[i]);
            break;
         case PTR_NODE::Detector: ((Detector *) _ptr)->_self.push_back(elements[i]);
            break;

         case PTR_NODE::AdcData: ((AdcData *) _ptr)->_self.push_back(elements[i]);
            break;

         case PTR_NODE::AdcInterval: ((AdcInterval *) _ptr)->_self.push_back(elements[i]);
            break;

         case PTR_NODE::Time:
            break;
      }
   }
}

TLigoFile::TLigoElements TLigoFile::FindChildByNode(TLigoFile::TLigoNode &node, TLigoFile::PTR_NODE validDataClass)
{
   TLigoFile::PTR_NODE dataClass;
   void *_ptr = NULL;

   std::tie(dataClass, _ptr) = node;
   return FindChildByNode(_ptr, dataClass, validDataClass);
}

TLigoFile::TLigoElements TLigoFile::FindChildByNode(void *_ptr, TLigoFile::PTR_NODE dataClass, TLigoFile::PTR_NODE validDataClass)
{
   TLigoElements elements;
   switch(dataClass)
   {
      case PTR_NODE::Empty     : 
         break;
      case PTR_NODE::LIGO_LW    :
         elements = ((LIGO_LW*) _ptr)->_self;
         break;
      case PTR_NODE::PCDATA     :
         break;
      case PTR_NODE::CDATA      :
         break;
      case PTR_NODE::Column      :
         break;
      case PTR_NODE::Table: 
         elements = ((Table *) _ptr)->_self;
         break;
      case PTR_NODE::Array: 
         elements = ((Array *) _ptr)->_self;
         break;
      case PTR_NODE::Stream:
         break;
      case PTR_NODE::IGWDFrame: 
         elements = ((IGWDFrame *) _ptr)->_self;
         break;
      case PTR_NODE::Detector: 
         elements = ((Detector *) _ptr)->_self;
         break;
      case PTR_NODE::AdcData: 
         elements = ((AdcData *) _ptr)->_self;
         break;
      case PTR_NODE::AdcInterval: 
         elements = ((AdcInterval *) _ptr)->_self;
         break;
      case PTR_NODE::Time:
         break;
      case PTR_NODE::Dim:
         break;
      case PTR_NODE::Comment:
         break;
      case PTR_NODE::Param:
         break;
   }

   TLigoElements filteredElements;
   for(int i = 0, N = elements.size(); i < N; i++) {

      if(elements[i].first != validDataClass) continue;

      filteredElements.push_back(elements[i]);
   }

   return filteredElements;
}

TLigoFile::TLigoNode TLigoFile::FindOneChildByNode(TLigoFile::TLigoNode &node, TLigoFile::PTR_NODE validDataClass)
{
   TLigoFile::PTR_NODE dataClass;
   void *_ptr = NULL;

   std::tie(dataClass, _ptr) = node;
   return FindOneChildByNode(_ptr, dataClass, validDataClass);
}

TLigoFile::TLigoNode TLigoFile::FindOneChildByNode(void *_ptr, TLigoFile::PTR_NODE dataClass, TLigoFile::PTR_NODE validDataClass)
{
   TLigoElements elements;
   switch(dataClass)
   {
      case PTR_NODE::Empty     : 
         break;
      case PTR_NODE::LIGO_LW    :
         elements = ((LIGO_LW*) _ptr)->_self;
         break;
      case PTR_NODE::PCDATA     :
         break;
      case PTR_NODE::CDATA      :
         break;
      case PTR_NODE::Column      :
         break;
      case PTR_NODE::Table: 
         elements = ((Table *) _ptr)->_self;
         break;
      case PTR_NODE::Array: 
         elements = ((Array *) _ptr)->_self;
         break;
      case PTR_NODE::Stream:
         break;
      case PTR_NODE::IGWDFrame: 
         elements = ((IGWDFrame *) _ptr)->_self;
         break;
      case PTR_NODE::Detector: 
         elements = ((Detector *) _ptr)->_self;
         break;
      case PTR_NODE::AdcData: 
         elements = ((AdcData *) _ptr)->_self;
         break;
      case PTR_NODE::AdcInterval: 
         elements = ((AdcInterval *) _ptr)->_self;
         break;
      case PTR_NODE::Time:
         break;
      case PTR_NODE::Dim:
         break;
      case PTR_NODE::Comment:
         break;
      case PTR_NODE::Param:
         break;
   }

   TLigoElements filteredElements;
   for(int i = 0, N = elements.size(); i < N; i++) {

      if(elements[i].first != validDataClass) continue;
      return elements[i];
   }

   return std::make_pair(TLigoFile::PTR_NODE::Empty, (void *) NULL);;
}

template<typename T, typename std::enable_if<std::is_same<T, REAL8FrequencySeries>::value || std::is_same<T, REAL4FrequencySeries>::value || std::is_same<T, REAL8TimeSeries>::value || std::is_same<T, REAL4TimeSeries>::value, int>::type>
T* TLigoFile::Cast(TLigoFile::TLigoNode &node)
{
   PTR_NODE dataClass;
   void *_ptr = NULL;

   std::tie(dataClass, _ptr) = node;
   if(_ptr == NULL) return NULL;

   if(dataClass != PTR_NODE::LIGO_LW) {
      std::cerr << TString("Unexpected data class received: " + enum2str(dataClass)) << std::endl;
      throw std::exception();
   }

   return Cast<T>((LIGO_LW *) node.second);
}

std::map<TString, TString> TLigoFile::GetParameters(TLigoFile::LIGO_LW *ligoLw)
{
   // Find parameters
   std::map<TString, TString> params;

   TLigoElements paramElements = TLigoFile::FindChildByNode(ligoLw, PTR_NODE::LIGO_LW, PTR_NODE::Param);
   for(int i = 0, N = paramElements.size(); i < N; i++) {

      Param *par = (Param *) paramElements[i].second;

      if(par != NULL) {

         TString name = ROOT::IOPlus::Helpers::Explode(":", par->Name.Data)[0];         
         TString data = ROOT::IOPlus::Helpers::Implode("", par->_self.Data).Strip(TString::kBoth, '\t');
               data = data.Strip(TString::kBoth);

         params.insert({name, data});
      }
   }

   return params;
}

template<typename T, typename std::enable_if<std::is_same<T, REAL8FrequencySeries>::value || std::is_same<T, REAL4FrequencySeries>::value || std::is_same<T, REAL8TimeSeries>::value || std::is_same<T, REAL4TimeSeries>::value, int>::type>
T* TLigoFile::Cast(TLigoFile::LIGO_LW *ligoLw)
{
   bool isROOT = TLigoFile::FindChildByNode(ligoLw, PTR_NODE::LIGO_LW, PTR_NODE::LIGO_LW).size() > 0;
   if ( isROOT ) return NULL;

   // Find parameters
   std::map<TString, TString> params = this->GetParameters(ligoLw);

   // Compute series
   Array *array = (Array *) TLigoFile::FindOneChildByNode(ligoLw, PTR_NODE::LIGO_LW, PTR_NODE::Array).second;
   T *series = Cast<T>(array);
      series->f0    = params["f0"].Atof();

   // Compute epoch
   Time *time = (Time *) TLigoFile::FindOneChildByNode(ligoLw, PTR_NODE::LIGO_LW, PTR_NODE::Time).second;

   double GPS = (time != NULL && time->Type == TimeType::GPS) ? ROOT::IOPlus::Helpers::Implode("", time->_self.Data).Atof() : 0;
   unsigned int GTimeS = (unsigned int) GPS;
   unsigned int GTimeN = 1e9 * (GPS - GTimeS);
   series->epoch = {static_cast<INT4>(GTimeS), static_cast<INT4>(GTimeN)};

   return series;
}

TLigoTable *TLigoFile::Cast(Table *table)
{
   TString tableName = ROOT::IOPlus::Helpers::Explode(":", table->Name.Data)[0];    

   TLigoTable *map = new TLigoTable(tableName);
   TLigoElements columns = TLigoFile::FindChildByNode((void *) table, PTR_NODE::Table, PTR_NODE::Column);
   Stream *stream = (Stream *) TLigoFile::FindOneChildByNode((void *) table, PTR_NODE::Table, PTR_NODE::Stream).second;

   for(int j = 0, J = columns.size(); j < J; j++)
   {
      Column *column = (Column *) columns[j].second;
      map->AddColumn(column->Name.Data, TLigoTable::str2type(column->Type.Data.Data()));
   }

   std::vector<std::vector<TString>> streamContent = Cast<TString>(stream);
   for(int j = 0, J = columns.size(); j < J; j++)
   {
      Column *column = (Column *) columns[j].second;
      for(int i = 0, I = streamContent.size(); i < I; i++) {

         std::vector<TString> entry = streamContent[i];
         map->Add(column->Name.Data.Data(), entry[j]);
      }
   }

   return map;
}

template<typename T, typename std::enable_if<std::is_same<T, REAL4FrequencySeries>::value, int>::type>
T* TLigoFile::Cast(TLigoFile::Array *array)
{
   TLigoElements dimElements = TLigoFile::FindChildByNode((void *) array, PTR_NODE::Array, PTR_NODE::Dim);

   // Get ordered axis
   std::vector<Dim *> dimAxis = this->Cast(array);
   if(dimAxis.size() > 2) {

      std::cerr << "Too many dimensions (" << dimElements.size() << ") received. Cannot convert LIGO Lightweight to ROOT format. This file is currently unsupported. (max dimension = 2)" << std::endl;
      throw std::exception();
   }

   // Cast stream into a series
   Stream *stream = (Stream *) TLigoFile::FindOneChildByNode((void *) array, PTR_NODE::Array, PTR_NODE::Stream).second;
   std::vector<std::vector<float>> axis = this->Axis<float>(stream);
   if(axis.size() < 2) {

      std::cerr << "Unexpected number of axis (" << axis.size() << ") found in array \"" << array->Name.Data << "\"" << std::endl;
      throw std::exception();
   }

   double start = dimAxis[0]->Start.Data.Atof();
   REAL4FrequencySeries *series = LALSuite::__REAL4FrequencySeries(array->Name.Data, axis[1], (axis[0].size() > 1 ? axis[0][1] - axis[0][0] : 0), start);
                        series->data->length -= 1;

   return series;
}

template<typename T, typename std::enable_if<std::is_same<T, REAL8FrequencySeries>::value, int>::type>
T* TLigoFile::Cast(TLigoFile::Array *array)
{
   TLigoElements dimElements = TLigoFile::FindChildByNode((void *) array, PTR_NODE::Array, PTR_NODE::Dim);

   // Get ordered axis
   std::vector<Dim *> dimAxis = this->Cast(array);
   if(dimAxis.size() > 2) {

      std::cerr << "Too many dimensions (" << dimElements.size() << ") received. Cannot convert LIGO Lightweight to ROOT format. This file is currently unsupported. (max dimension = 2)" << std::endl;
      throw std::exception();
   }

   // Cast stream into a series
   Stream *stream = (Stream *) TLigoFile::FindOneChildByNode((void *) array, PTR_NODE::Array, PTR_NODE::Stream).second;
   std::vector<std::vector<double>> axis = this->Axis<double>(stream);
   if(axis.size() < 2) {

      std::cerr << "Unexpected number of axis (" << axis.size() << ") found in array \"" << array->Name.Data << "\"" << std::endl;
      throw std::exception();
   }

   double start = dimAxis[0]->Start.Data.Atof();
   REAL8FrequencySeries *series = LALSuite::__REAL8FrequencySeries(array->Name.Data, axis[1], (axis[0].size() > 1 ? axis[0][1] - axis[0][0] : 0), start);
                        series->data->length -= 1;

   return series;
}

template<typename T, typename std::enable_if<std::is_same<T, REAL4TimeSeries>::value, int>::type>
T* TLigoFile::Cast(TLigoFile::Array *array)
{
   TLigoElements dimElements = TLigoFile::FindChildByNode((void *) array, PTR_NODE::Array, PTR_NODE::Dim);

   // Get ordered axis
   std::vector<Dim *> dimAxis = this->Cast(array);
   if(dimAxis.size() > 2) {

      std::cerr << "Too many dimensions (" << dimElements.size() << ") received. Cannot convert LIGO Lightweight to ROOT format. This file is currently unsupported. (max dimension = 2)" << std::endl;
      throw std::exception();
   }

   // Cast stream into a series
   Stream *stream = (Stream *) TLigoFile::FindOneChildByNode((void *) array, PTR_NODE::Array, PTR_NODE::Stream).second;
   std::vector<std::vector<float>> axis = this->Axis<float>(stream);
   if(axis.size() < 2) {

      std::cerr << "Unexpected number of axis (" << axis.size() << ") found in array \"" << array->Name.Data << "\"" << std::endl;
      throw std::exception();
   }
   
   double start = dimAxis[0]->Start.Data.Atof();
   return LALSuite::__REAL4TimeSeries(array->Name.Data, axis[1], (axis[0].size() > 1 ? axis[0][1] - axis[0][0] : 0), start);
}

template<typename T, typename std::enable_if<std::is_same<T, REAL8TimeSeries>::value, int>::type>
T* TLigoFile::Cast(TLigoFile::Array *array)
{
   TLigoElements dimElements = TLigoFile::FindChildByNode((void *) array, PTR_NODE::Array, PTR_NODE::Dim);

   // Get ordered axis
   std::vector<Dim *> dimAxis = this->Cast(array);
   if(dimAxis.size() > 2) {

      std::cerr << "Too many dimensions (" << dimElements.size() << ") received. Cannot convert LIGO Lightweight to ROOT format. This file is currently unsupported. (max dimension = 2)" << std::endl;
      throw std::exception();
   }

   // Cast stream into a series
   Stream *stream = (Stream *) TLigoFile::FindOneChildByNode((void *) array, PTR_NODE::Array, PTR_NODE::Stream).second;
   std::vector<std::vector<double>> axis = this->Axis<double>(stream);
   if(axis.size() < 2) {

      std::cerr << "Unexpected number of axis (" << axis.size() << ") found in array \"" << array->Name.Data << "\"" << std::endl;
      throw std::exception();
   }

   double start = dimAxis[0]->Start.Data.Atof();
   REAL8TimeSeries *series = LALSuite::__REAL8TimeSeries(array->Name.Data, axis[1], (axis[0].size() > 1 ? axis[0][1] - axis[0][0] : 0), start);
                  series->data->length -= 1;

   return series;
}



std::vector<TLigoFile::Dim *> TLigoFile::Cast(TLigoFile::Array *array)
{
   TLigoElements dimElements = TLigoFile::FindChildByNode((void *) array, PTR_NODE::Array, PTR_NODE::Dim);

   // Find reference axis entry
   Dim *refAxis = NULL;
   for(int i = 0, N = dimElements.size(); i < N; i++) {

         Dim *dim = (Dim *) dimElements[i].second;
         if(dim->Name.Data.Contains(",")) refAxis = dim;
   }

   // Get ordered axis
   std::vector<Dim *> dimAxis;
   std::vector<TString> axisOrder = ROOT::IOPlus::Helpers::Explode(",", refAxis->Name.Data);
   for(int i = 0, I = axisOrder.size(); i < I; i++) {

      Dim *dim = NULL;
      for(int j = 0, J = dimElements.size(); j < J; j++) {
         
         Dim *_dim = (Dim *) dimElements[j].second;
         if(_dim->Name.Data.Contains(axisOrder[i])) {
            dim = _dim;
            break;
         }
      }
      
      if(dim == NULL) {

         dim = new Dim;
         dim->Name.Data  = axisOrder[i];
         dim->Unit.Data  = "";
         dim->Start.Data = "0";
         dim->Scale.Data = "1";
      }

      dimAxis.push_back(dim);
   }

   return dimAxis;
}
template<typename T>
std::vector<std::vector<T>> TLigoFile::Cast(TLigoFile::Stream *stream)
{
   std::vector<std::vector<T>> x;

   if(stream == NULL) return x;
   const char separator = stream->Delimiter.Data[0];
   
   for(int i = 0, I = stream->_self.Data.size(); i < I; i++) {

      TString data = stream->_self.Data[i];
            data = data.Strip(TString::kBoth, '\t');
            data = data.Strip(TString::kBoth);

      if(data.EqualTo("")) continue;
      
      x.push_back(ROOT::IOPlus::Helpers::Explode(separator, data));
   }
   
   return x;
}

template<typename T>
std::vector<std::vector<T>> TLigoFile::Axis(TLigoFile::Stream *stream)
{
   std::vector<std::vector<T>> x;

   if(stream == NULL) return x;
   const char separator = stream->Delimiter.Data[0];
   
   std::vector<T> dx;
   for(int i = 0, I = stream->_self.Data.size(); i < I; i++) {

      TString data = stream->_self.Data[i];
            data = data.Strip(TString::kBoth, '\t');
            data = data.Strip(TString::kBoth);

      if(data.EqualTo("")) continue;
      std::vector<TString> v = ROOT::IOPlus::Helpers::Explode(separator, data);
      
      if(x.size() < v.size()) x.resize(v.size());

      for(int j = 0, J = v.size(); j < J; j++) {

         int i0 = x[j].size()-1;
         
         x[j].push_back(v[j].Atof());
         if(j == J-1 || i0 == 0) continue;

         if(dx.size() < x.size()) dx.push_back(x[j][1] - x[j][0]);

         if(!ROOT::IOPlus::Helpers::EpsilonEqualTo(dx[j], x[j][i0-1] - x[j][i0-1])) {

            std::cout << "Unexpected bin width provided for \"" << stream->Name.Data << "\" axis #" << j << " .. " << dx[j] << " != " << (x[j][i0] - x[j][i0-1]) << std::endl;
            throw std::exception();
         }
      }

      // 
      // Add an additional bin at end due to bin edging
      if(i == I-2) {

         x[x.size()-1].push_back(0);
         for(int j = 0, J = x.size(); j < J-1; j++) {
            x[j].push_back(x[j][x[j].size()-1] + dx[j]);
         }
      }
   }
   
   return x;
}

TDirectory *TLigoFile::GetRelativeIndexDirectory(LIGO_LW *ligoLw, std::vector<TString> indexers, TDirectory *relativeDir)
{
   TDirectory *gParentDirectory = gDirectory;
   TDirectory *gIndexDirectory  = relativeDir == NULL ? gDirectory : relativeDir;

   std::map<TString, TString> parameters = this->GetParameters(ligoLw);
   if(!indexers.size()) {

      for(typename std::map<TString, TString>::const_iterator it = parameters.begin(); it != parameters.end(); ++it) {

         if(it->first.EqualTo("f0")) continue;
         indexers.push_back(it->first);
      }
   }

   for(int i = 0, N = indexers.size(); i < N; i++) {

      TString index = ROOT::IOPlus::Helpers::Explode(":", indexers[i])[0];

      TString parameter = parameters[index];
      if(parameter.EqualTo("")) 
         gPrint->Warning(__METHOD_NAME__, "Indexation parameter `%s` not found for %s", index.Data(), ligoLw->Name.Data.Data());

      TDirectory *dir = gIndexDirectory->mkdir(index+"/"+parameters[index]);
      if(dir) gDirectory->cd(index+"/"+parameters[index]);
      
      gIndexDirectory = gDirectory;
   }

   gParentDirectory->cd();

   return gIndexDirectory;
}

bool TLigoFile::Parse(const char *fname, TString title, TString pattern, std::vector<TString> indexers)
{
   TFileReader fReader("reader", "f", fname);

   for(int i = 0, N = fReader.GetN(); i < N; i++) {
      
      if(fReader.Get(i)->InheritsFrom(TLigoTable::Class()->GetName())) {

         TLigoTable *map = (TLigoTable *) fReader.Get(i);
         gDirectory->Add(map);

      } else if (fReader.Get(i)->InheritsFrom("TH1")) {

         TH1 *h = (TH1*) fReader.Get(i);
            h->GetDirectory()->cd();

         TDirectory *gParentDirectory = gDirectory;

         TDirectory *gLightweightDirectory = this->GetDirectory("/");
         if(title.EqualTo("")) {

            while(gDirectory->GetMotherDir() != NULL) {

                  title = TString("/") + gDirectory->GetName() + title;
                  gDirectory->cd("..");
            }

            title = TString(title.Strip(TString::kBoth, '/'));
            gLightweightDirectory->mkdir(title);
            gLightweightDirectory->cd(title);
         }
         
         if (!title.EqualTo("")) {

            TDirectory *gLightweightDirectory = this->GetDirectory("/");

            gLightweightDirectory->mkdir(title);
            gLightweightDirectory->cd(title);
         }

         //
         // Extra parameters & directory structure
         std::vector<TString> extraParameters = pattern.EqualTo("") ? std::vector<TString>({}) : TFileReader::ParseIdentifier(fname, pattern);
         for(int i = 0, N = TMath::Min(extraParameters.size(), indexers.size()); i < N; i++) {
            
            TDirectory *dir = NULL;
            
            dir = gDirectory->mkdir(indexers[i]);
            if(dir) dir->cd();
            dir = gDirectory->mkdir(extraParameters[i]);
            if(dir) dir->cd();
         }

         h->SetDirectory(gDirectory);
         gParentDirectory->cd();

      } else {

         std::cerr << "Unsupported TObject provided.. skip" << std::endl; 
      }
   }

   return 1;
}

bool TLigoFile::ParseXML(const char *fname, TString title, TString pattern, std::vector<TString> indexers)
{
   UNUSED(pattern); // Pattern not used.. but might be implemented later maybe, if needed?

   TString objName = TFileReader::Obj(fname);
         objName.ReplaceAll("(", "");
         objName.ReplaceAll(")", "");
         objName.ReplaceAll(".*", "*");

   if(objName.EqualTo("/")) objName = "";

   if(!TFileReader::IsXmlFile(fname)) {

         gPrint->Error(__METHOD_NAME__, "Input file is expected to be in XML format.. `%s`", fname);
         return false;
   }
   
   if(!TFileReader::Exists(fname)) {
         gPrint->Error(__METHOD_NAME__, "Input file not found.. `%s`", fname);
         return false;
   }

   //@WARN TO Implement once TConfig Parser is ready....
   
   // TConfigParser *api = TConfigParser::Open(TFileReader::FileName(fname));
   // TXMLDocument *xml = api->GetXML();

   // if(xml != nullptr) {

   //    // Prepare XML root element
   //    TLigoFile::PTR_NODE dataClass;
   //    void *ptr = NULL;
      
   //    std::tie(dataClass, ptr) = ParseXMLNode(xml->GetRootNode());

   //    if(ptr == NULL) return false;

   //    if(dataClass != TLigoFile::PTR_NODE::LIGO_LW) {
   //       std::cerr << TString("Unexpected data class received: " + enum2str(dataClass)) << std::endl;
   //       throw std::exception();
   //    }

   //    // Compute its elements
   //    LIGO_LW *rootElement   = (LIGO_LW *) dataClass;
   //    TLigoElements elements = rootElement->_self;

   //    TDirectory *gParentDirectory = gDirectory;
   //    if (!rootElement->Name.Data.EqualTo("")) {
   //       TDirectory *dir = gDirectory->mkdir(rootElement->Name.Data);
   //       if(dir) dir->cd();
   //    }

   //    for(int i = 0, N = elements.size(); i < N; i++) {

   //       switch(elements[i].first) {
      
   //          case PTR_NODE::Table:
   //          {
            
   //             Table *table = ((Table*) elements[i].second);
   //             TString tableName = ROOT::IOPlus::Helpers::Explode(":", table->Name.Data)[0];      

   //             bool match = true;
   //             if(!objName.EqualTo("")) {
               
   //                TString path = TFileReader::Obj(TString(gDirectory->GetPath()) + "/" + tableName);
   //                TPRegexp rgx(objName+"$");
   //                match = rgx.MatchB(path);
   //             }

   //             if(match) {

   //                TLigoTable *tableMap = this->Cast(table);
   //                            tableMap->SetTitle(title);

   //                gDirectory->Add(tableMap);
   //             }

   //             break;
   //          }

   //          case PTR_NODE::LIGO_LW:
   //          {
   //             LIGO_LW *element = (LIGO_LW*) elements[i].second;

   //             bool match = true;
   //             if(!objName.EqualTo("")) {
                  
   //                TString path = TFileReader::Obj(TString(gDirectory->GetPath()) + "/" + rootElement->Name.Data);
   //                TPRegexp rgx(objName+"$");
   //                match = rgx.MatchB(path);
   //             }

   //             if(match) {

   //                TDirectory *gRootDirectory = gDirectory;
   //                TDirectory *gIndexDirectory = this->GetRelativeIndexDirectory(element, indexers);
   //                            gIndexDirectory->cd();

   //                switch(str2series(element->Name.Data)) {

   //                   case Series::REAL4TimeSeries: 
   //                   {
   //                      REAL4TimeSeries *series = this->Cast<REAL4TimeSeries>(elements[i]);
   //                      TH1* h = LALSuite::__TH1(series);
   //                         h->SetName(series->name);
   //                         h->SetDirectory(gDirectory);

   //                      break;
   //                   }

   //                   case Series::REAL4FrequencySeries:
   //                   {
   //                      REAL4FrequencySeries *series = this->Cast<REAL4FrequencySeries>(elements[i]);
   //                      TH1* h = LALSuite::__TH1(series);
   //                         h->SetName(series->name);
   //                         h->SetDirectory(gDirectory);

   //                      break;
   //                   }

   //                   case Series::REAL8TimeSeries: 
   //                   {
   //                      REAL8TimeSeries *series = this->Cast<REAL8TimeSeries>(elements[i]);
   //                      TH1* h = LALSuite::__TH1(series);
   //                         h->SetName(series->name);
   //                         h->SetDirectory(gDirectory);

   //                      break;
   //                   }

   //                   case Series::REAL8FrequencySeries:
   //                   {
   //                      REAL8FrequencySeries *series = this->Cast<REAL8FrequencySeries>(elements[i]);
   //                      TH1* h = LALSuite::__TH1(series);
   //                         h->SetName(series->name);
   //                         h->SetDirectory(gDirectory);

   //                      break;
   //                   }

   //                   default:
   //                      std::cerr << "\"" << element->Name.Data << "\" is not supported yet." << std::endl;
   //                      throw std::exception();
   //                }
         
   //                gRootDirectory->cd();
   //             }

   //             break;
   //          }

   //          default:
   //             std::cerr << "\"" << enum2str(elements[i].first) << "\" node is not supported yet." << std::endl;
   //             throw std::exception();
   //       }
   //    }

   //    gParentDirectory->cd();
   // }

   return true;
}

int TLigoFile::Write(const char *fname, int option, int bufsiz)
{
   UNUSED(option);
   
   if (!IsWritable()) {
      if (!TestBit(kWriteError)) {
         // Do not print the warning if we already had a SysError.
         Warning("Write", "file %s not opened in write mode", GetName());
      }
      return 0;
   }

   if (gDebug) {
      if (!GetTitle() || strlen(GetTitle()) == 0)
         Info("Write", "writing name = %s", GetName());
      else
         Info("Write", "writing name = %s title = %s", GetName(), GetTitle());
   }

   fMustFlush = kFALSE;
   Int_t nbytes = TDirectoryFile::Write(fname, TObject::kSingleKey, bufsiz); // Write directory tree
   WriteStreamerInfo();
   WriteFree();                       // Write free segments linked list
   WriteHeader();                     // Now write file header
   fMustFlush = kTRUE;

   return nbytes;
}

int TLigoFile::WriteXML(const char *xmlFile, LIGOLwXMLStream *xml, TDirectory *directory)
{
   int nbytes = 0;
   if(directory == NULL) return nbytes;

   TList *l = directory->GetList();
   TIter Next(l);
   
   TObject *obj = NULL;
   while ( (obj = Next()) ) {

      if(obj->InheritsFrom("TDirectory")) nbytes += WriteXML(xmlFile, xml, (TDirectory *) obj);
      else if(obj->InheritsFrom("TH1")  ) nbytes += WriteXML(xmlFile, xml, (TH1*) obj); 
      else if(obj->InheritsFrom(TLigoTable::Class()->GetName()) ) nbytes += WriteXML(xmlFile, xml, (TLigoTable*) obj); 
   }

   return nbytes;
}

int TLigoFile::WriteXML(const char *xmlFile, LIGOLwXMLStream *xml, TLigoTable *tableMap)
{
   UNUSED(xmlFile);
   
   int nbytes = 0;
   if(tableMap == NULL) return nbytes;

   const char *row_head = "\n\t\t\t";

   std::vector<TString> columnNames = tableMap->GetColumns();

   /* table header */
   XLALClearErrno();
   XLALFilePuts("\t<Table Name=\""+TString(tableMap->GetName())+"\">\n", xml->fp);

   int entries = tableMap->GetEntries();
   for(int i = 0, N = columnNames.size(); i < N; i++) {

      TString key = columnNames[i];
      TLigoTable::Type type = tableMap->GetType(key);
      
      XLALFilePuts("\t\t<Column Name=\""+key+"\" Type=\""+TLigoTable::enum2str(type)+"\"/>\n", xml->fp);
   }

   XLALFilePuts("\t\t<Stream Name=\""+TString(tableMap->GetName())+"\" Type=\"Local\" Delimiter=\",\">", xml->fp);

   if(XLALGetBaseErrno())
      XLAL_ERROR(XLAL_EFUNC);

   for(int i = 0; i < entries; i++) {

      gPrint->ProgressBar(Form("Computing LIGO Lightweight table.."), "", i+1, entries);

      TString line;
      for(int j = 0, N = columnNames.size(); j < N; j++) {

         TString key = columnNames[j];
         TString value = tableMap->Get(key, i);
         TLigoTable::Type type = tableMap->GetType(key);

         if(j > 0) line += ",";
         if(type == TLigoTable::Type::lstring) line += "\"";
         line += value.Data();
         if(type == TLigoTable::Type::lstring) line += "\"";
      }

      if(XLALGetBaseErrno())
         XLAL_ERROR(XLAL_EFUNC);

      if(XLALFilePuts(Form("%s%s", row_head, line.Data()), xml->fp) < 0 ) 
         XLAL_ERROR(XLAL_EFUNC);

      row_head = ",\n\t\t\t";
   }

   if(XLALFilePuts("\n\t\t</Stream>\n\t</Table>\n", xml->fp) < 0)
      XLAL_ERROR(XLAL_EFUNC);

   return nbytes;   
}

int TLigoFile::WriteXML(const char *xmlFile, LIGOLwXMLStream *xml, TH1 *h)
{
   int nbytes = 0;
   if(h == NULL) return nbytes;

   std::vector<TString> metadata = ROOT::IOPlus::Helpers::Explode(' ', h->GetTitle());
   TLigoFile::Series type = str2series(metadata[0]); 

   int ret = -1;
   switch(type) {

      case Series::REAL4TimeSeries: 
      {
         ret = XLALWriteLIGOLwXMLArrayREAL4TimeSeries(xml, h->GetTitle(), LALSuite::__REAL4TimeSeries((TH1F *) h));
         break;
      }

      case Series::REAL8TimeSeries: 
      {
         ret = XLALWriteLIGOLwXMLArrayREAL8TimeSeries(xml, h->GetTitle(), LALSuite::__REAL8TimeSeries((TH1D *) h));
         break;
      }

      case Series::REAL4FrequencySeries: 
      {
         ret = XLALWriteLIGOLwXMLArrayREAL4FrequencySeries(xml, h->GetTitle(), LALSuite::__REAL4FrequencySeries((TH1F *) h));
         break;
      }

      case Series::REAL8FrequencySeries: 
      {
         ret = XLALWriteLIGOLwXMLArrayREAL8FrequencySeries(xml, h->GetTitle(), LALSuite::__REAL8FrequencySeries((TH1D *) h));
         break;
      }

      // case Series::COMPLEX16TimeSeries: 
      //    ret = XLALWriteLIGOLwXMLArrayCOMPLEX16TimeSeries(xml, h->GetTitle(), LALSuite::__COMPLEX16TimeSeries(h));
      //    break;

      // case Series::COMPLEX8TimeSeries: 
      //    ret = XLALWriteLIGOLwXMLArrayCOMPLEX8TimeSeries(xml, h->GetTitle(), LALSuite::__COMPLEX8TimeSeries(h));
      //    break;

      // case Series::COMPLEX16FrequencySeries: 
      //    ret = XLALWriteLIGOLwXMLArrayCOMPLEX16FrequencySeries(xml, h->GetTitle(), LALSuite::__REAL4TimeSeries(h));
      //    break;

      // case Series::COMPLEX8FrequencySeries: 
      //    ret = XLALWriteLIGOLwXMLArrayCOMPLEX8FrequencySeries(xml, h->GetTitle(), LALSuite::__REAL4TimeSeries(h));
      //    break;

      default:
         break;
   }

   if(ret < 0) gPrint->Warning(__METHOD_NAME__, "Failed to write `%s` into %s (not supported yet)", h->GetName(), xmlFile);   
   else nbytes += sizeof(h);

   //
   // Writing extra indexation parameters into XML
   ::XLALFileSeek(xml->fp, -TString("</Array>\t\t\t\n").Length(), SEEK_CUR);

   TDirectory *gParentDirectory = gDirectory;
   h->GetDirectory()->cd();
   
   while(gDirectory->GetMotherDir() != NULL) {

      TString parameter = gDirectory->GetName();
      gDirectory->cd("..");
      TString indexer = gDirectory->GetName();
      gDirectory->cd("..");
      
      if(indexer.EqualTo("f0")) continue; // Already included when writing <Array>
      if(TFileReader::Exists(parameter) || TFileReader::Exists(indexer))
         break;

      bool error = ::XLALFilePrintf(xml->fp, "\t\t<Param Name=\"%s\" Type=\"lstring\">%s</Param>\n", indexer.Data(), parameter.Data()) < 0;      
      if(error) gPrint->Error(__METHOD_NAME__, "Failed to write parameter `%s` related to array `%s`", indexer.Data(), h->GetName());
      else nbytes += parameter.Length();
   }
   
   ::XLALFilePrintf(xml->fp, "\t</LIGO_LW>\n");      
      
   gParentDirectory->cd();
   return nbytes;
}

int TLigoFile::WriteXML(const char *xmlFile)
{
   int nbytes = 0;

   TList *list = this->GetDirectory("/")->GetList();
   TIter Next(list);

   LIGOLwXMLStream *xml = XLALOpenLIGOLwXMLFile(xmlFile);
   TString title = "";
   
   TObject *obj = NULL;
   while ( (obj = Next()) )
   {
      if(obj->InheritsFrom("TDirectory")) {
         
         title = obj->GetName();
         nbytes += WriteXML(xmlFile, xml, (TDirectory *) obj);
         
      } else if(obj->InheritsFrom("TH1")) {
   
         nbytes += WriteXML(xmlFile, xml, (TH1*) obj); 

      } else if(obj->InheritsFrom(TLigoTable::Class()->GetName())) {
   
         nbytes += WriteXML(xmlFile, xml, (TLigoTable*) obj); 
      }
   }

   XLALCloseLIGOLwXMLFile(xml);

   //
   // Write title into <LIGO_LW> root element..
   std::vector<TString> regex;
   std::vector<TString> replacement;

   if(!title.EqualTo("")) {
      regex.push_back("<LIGO_LW>");
      replacement.push_back("<LIGO_LW Name=\""+title+"\">");
   }

   if(!this->DTD.EqualTo("")) {
      regex.push_back("<!DOCTYPE LIGO_LW SYSTEM .*>");
      replacement.push_back("<!DOCTYPE LIGO_LW SYSTEM \""+this->DTD+"\">");
   }

   gPrint->ReadAndWrite(xmlFile, regex, replacement);
   return nbytes;
}
