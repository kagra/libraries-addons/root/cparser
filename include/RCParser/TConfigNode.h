#pragma once

#include <Rioplus.h>
#include <TPrint.h>

#include <TString.h>
#include <variant>
#include <map>
#include <functional>
#include <unordered_map>

// Define a custom hash function for TString
namespace std {
    template <>
    struct hash<TString> {
        size_t operator()(const TString& str) const {
            // Use the hash value of the string data
            return hash<string>{}(str.Data());
        }
    };

    // Define a custom equality comparison for TString
    template <>
    struct equal_to<TString> {
        bool operator()(const TString& lhs, const TString& rhs) const {
            // Use the CompareTo method to check equality
            return lhs.CompareTo(rhs) == 0;
        }
    };
}

class TConfigNode {

    // Class enums and aliases
    public:
        enum class Insert { Before, After };
        enum class TraversalOrder { PreOrder, InOrder, PostOrder };
        enum class Type { Empty, Null, Boolean, Integer, Float, Double, String };
        enum class Indexer { None, Array, Dictionary };
        
        inline static TString enum2str(Type type) {
            switch(type) {
                case Type::Null:
                    return "null";
                case Type::Boolean:
                    return "bool";
                case Type::Integer:
                    return "int";
                case Type::Float:
                    return "float";
                case Type::Double:
                    return "double";
                case Type::String:
                    return "string";
                default:
                case Type::Empty:
                    return "empty";
            }

            return "";
        }

    // Iterator feature
    public:
        std::vector<TConfigNode*>::iterator begin() { return elements.begin(); }
        std::vector<TConfigNode*>::iterator end() { return elements.end(); }

    // Proxy auto-typing
    public:

        using Value = std::variant<
            std::monostate,
            std::nullptr_t,
            bool, 
            int,
            float,
            double,
            TString
        >;

        template<typename T>
        operator T() const {

            try {
                
                if(std::get_if<std::monostate>(&content))
                {
                    if constexpr (std::is_same_v<T, std::nullptr_t>) return nullptr;
                    else if constexpr (std::is_same_v<T, bool>) return false;
                    else if constexpr (std::is_same_v<T, int>) return 0;
                    else if constexpr (std::is_same_v<T, float>) return NAN;
                    else if constexpr (std::is_same_v<T, double>) return NAN;
                    else if constexpr (std::is_same_v<T, TString>) return "";
                    
                    TString what = (TString) "[TConfigNode] Unexpected type requested for `"+this->GetTag()+"` (key not found)";
                    throw std::runtime_error(what.Data());
                }

                if constexpr (std::is_same_v<T, double>) {
                    if (this->GetType() == Type::Float)   return (double) std::get<float>(this->GetContent());
                    if (this->GetType() == Type::Integer) return (double) std::get<float>(this->GetContent());
                }

                if constexpr (std::is_same_v<T, float>) {
                    if (this->GetType() == Type::Double)  return (float) std::get<double>(this->GetContent());
                    if (this->GetType() == Type::Integer) return (float) std::get<int>(this->GetContent());
                }

                if constexpr (std::is_same_v<T, int>) {
                    if (this->GetType() == Type::Double) return (int) std::get<float>(this->GetContent());
                    if (this->GetType() == Type::Float)  return (int) std::get<double>(this->GetContent());
                }
                
                if constexpr (std::is_same_v<T, bool>) {
                    if (this->GetType() == Type::Integer) return std::get<int>(this->GetContent()) == 0;
                }

                if constexpr (std::is_same_v<T, TString>) {
                    return this->AsString();
                }
                
                return std::get<T>(this->GetContent());

            } catch (const std::bad_variant_access& e) {
                TString what = (TString) "[TConfigNode] Failed to cast `"+this->GetTag()+"` into a " + TPrint::GetType<T>() + " (maybe use `auto` instead)";
                throw std::runtime_error(what.Data());
            }
        }

        friend std::ostream& operator<<(std::ostream& os, const TConfigNode& node) {

            std::visit([&os](const auto& value) {

                if constexpr (!std::is_same_v<decltype(value), const std::monostate&>) {

                    if constexpr (std::is_same_v<decltype(value), TString>)
                        if(value.Length()-4 > 10) os << value(0, 10) << "[..]";
                        else os << value;
                    else
                        os << value;
                }

            }, node.GetContent());

            return os;
        }
    
    protected:
        TString tag;
        TConfigNode* parent = nullptr;

        Indexer indexer = Indexer::None;
        Value content = std::monostate{};

        std::unordered_map<TString, TString> attributes;
        std::vector<TConfigNode*> elements;
        
        bool IsAncestorOf(TConfigNode& node) const;

    public:

        TConfigNode();
        ~TConfigNode();

        explicit TConfigNode(const TString& tag);
        TConfigNode(const TConfigNode &node);

        bool operator==(const TConfigNode& other) const {
            
            bool sameElements = (this->elements.size() == other.elements.size());
            for (size_t i = 0; i < this->elements.size(); ++i) {
                
                if(!sameElements) break;
                if(this->elements[i] == other.elements[i]) continue;
                if(this->elements[i] == nullptr) sameElements = false;
                if(other.elements[i] == nullptr) sameElements = false;

                TConfigNode &node1 = *(this->elements[i]);
                TConfigNode &node2 = *(other.elements[i]);
                sameElements &= !(node1 == node2);
            }
            
            return  tag == other.tag && 
                    content == other.content &&
                    attributes == other.attributes && 
                    parent == other.parent && sameElements;
        }

        bool operator!=(const TConfigNode& other) const {
            return !(*this == other);
        }

        TString GetPath() const;

        TConfigNode * GetParent() const;
        std::vector<TConfigNode *> GetElements() const;
        std::unordered_map<TString, TString> GetAttributes() const;

        bool Has(int index);
        bool Has(const char *element);
        TConfigNode &Get(int index);
        TConfigNode &Get(const char *element);
        TConfigNode &operator[](int index);
        TConfigNode &operator[](const char *element);

        void Attach(TConfigNode& element);
        void Attach(TConfigNode& element, Insert position, const TConfigNode& specificElement);
        void Detach(TConfigNode& element);

        Type GetType() const;
        int GetN() const;

        Indexer GetIndexer() const;
        void SetIndexer(Indexer indexer);
        bool IsDictionary() const;
        bool IsArray() const;
        bool IsEmpty() const;
        
        TString AsString() const;
        Value GetContent() const;
        void SetContent(const Value &content);
        void ClearContent();
        TConfigNode& operator=(const Value& content);

        TString GetTag() const;
        void SetTag(TString tag);

        int GetDepth() const;

        bool IsSelfClosing() const;
        void Walk(TraversalOrder order, std::function<void(const TString&, const TConfigNode&, const int)> callback) const;
        TConfigNode* Next();

        bool HasAttribute(const TString& key) const;
        TString GetAttribute(const TString& key) const;
        void AddAttribute(const TString& key, const TString& value);
        void RemoveAttribute(const TString& key);

        void Print(Option_t * = "");
        void Dump(Option_t * = "") const;
        void DumpAll() const { return Dump("pre"); }
 
    protected:
        void PreOrderWalk(std::function<void(const TString&, const TConfigNode&, const int)> callback) const;
        void InOrderWalk(std::function<void(const TString&, const TConfigNode&, const int)> callback) const;
        void PostOrderWalk(std::function<void(const TString&, const TConfigNode&, const int)> callback) const;
};