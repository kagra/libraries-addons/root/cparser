#pragma once

#include <Rioplus.h>
#include <TCurl.h>

#include <TString.h>
#include <TUrl.h>

#include <fstream>

#include "RCParser/TConfigAdapter.h"
#ifdef INI_FOUND
    #include "RCParser/Adapter/TConfigIni.h"
#endif
#ifdef JSON_FOUND
    #include "RCParser/Adapter/TConfigJson.h"
#endif
#ifdef YAML_FOUND
    #include "RCParser/Adapter/TConfigYaml.h"
#endif
#ifdef TOML_FOUND
    #include "RCParser/Adapter/TConfigToml.h"
#endif
#ifdef XML_FOUND
    #include "RCParser/Adapter/TConfigXml.h"
#endif

#include "RCParser/TConfigNode.h"

class TConfigParser: public TConfigNode {

    protected:

        TString fName, adapterName;
        TConfigNode::TraversalOrder traversalOrder;

        static std::vector<std::unique_ptr<TConfigAdapter>> adapters;

    public:

        TConfigParser(TString filename, Option_t *opt, int ttl = 0, const char *adapterName = "");
        TConfigParser(TString filename,                int ttl = 0, const char *adapterName = ""): TConfigParser(filename, "RPF", ttl, adapterName) {}
        TConfigParser(TConfigNode *node, const char* adapterName = "");

        ~TConfigParser();

        static void InitializeAdapters() {

            if (TConfigParser::adapters.size())
                return;
    
            #ifdef INI_FOUND
            adapters.push_back(std::make_unique<TConfigIni>());
            #endif

            #ifdef JSON_FOUND
            adapters.push_back(std::make_unique<TConfigJson>());
            #endif
        
            #ifdef YAML_FOUND
            adapters.push_back(std::make_unique<TConfigYaml>());
            #endif
        
            #ifdef TOML_FOUND
            adapters.push_back(std::make_unique<TConfigToml>());
            #endif
        
            #ifdef XML_FOUND
            adapters.push_back(std::make_unique<TConfigXml>());
            #endif
        }

        void SetTraversalOrder(TConfigNode::TraversalOrder traversalOrder)
        {
            this->traversalOrder = traversalOrder;
        }
        
        static TConfigNode *Parse(const char *contents, const char *adapterName);

        void Write(const char *filename, const char *adapterName = "");
        void Print(Option_t* opt = "");
        inline void DumpAll() { return TConfigNode::Dump("pre"); }
               void Dump(Option_t *opt = "");
};