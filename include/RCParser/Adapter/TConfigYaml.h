#pragma once 

#include <RCParser/TConfigAdapter.h>

#ifdef YAML_FOUND

// Concrete adapter class for YAML format
class TConfigYaml : public TConfigAdapter {

    public:

        TConfigNode *Parse(const char *contents, int size) override;
        
        const char *GetName() override { return "yaml"; }
        void Save(const char *filename, const TConfigNode& root) override;
        bool Supports(const char *filename) override;
};

#endif
