#pragma once 

#include <RCParser/TConfigAdapter.h>
#include <TString.h>

#ifdef INI_FOUND

/**
 * @brief Concrete adapter class for INI format
 * @extends TConfigAdapter
 */
class TConfigIni : public TConfigAdapter {

    public:

       /**
        * @brief Parse the contents into a TConfigNode
        * @param contents The content to be parsed
        * @param size The size of the content
        * @return Parsed TConfigNode
        * @override
        */
        TConfigNode *Parse(const char *contents, int size) override;

        const char *GetName() override { return "ini"; }
        void Save(const char *filename, const TConfigNode& root) override;
        bool Supports(const char *filename) override;
};

#endif
