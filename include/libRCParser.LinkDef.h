/**
 **********************************************
 *
 * \file libRCParser.LinkDef.h
 * \brief Link definitions of the library
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#ifdef __CINT__
#include "libRCParser.Config.h"

#pragma link off all globals;
#pragma link off all functions;

#pragma link C++ class TConfigParser+;
#pragma link C++ class TConfigAdapter+;
#pragma link C++ class TConfigNode+;

#ifdef LALSuite_FOUND
    #pragma link C++ class TLigoTable+;
    #pragma link C++ class TLigoFile+;
#endif

#endif