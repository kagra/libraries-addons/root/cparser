#include <Rioplus.h>

#include "TConfig.h"

int main() {

    // Example usage
    TConfigNode config("root");
                config.Attach(config); // warning expected.

    TConfigNode key1("key1");
    TConfigNode key11("key11");
    TConfigNode key12("key12");
    TConfigNode key13("key13");
    TConfigNode key2("key2");
    TConfigNode key3("key3");

    key1.Attach(key11);
    key1.Attach(key12);
    key1.Attach(key13);
    key1.Attach(key13);
    
    key11 = "Test Content";
    
    key2.AddAttribute("attribute1", "attr_value1");
    key2.AddAttribute("subkey1_attr", "attr_value2");

    config.Attach(key1);
    config.Attach(key2);
    config.Attach(key3);

    key1.Detach(key12);

    std::cout << "* PreOrder walk:" << std::endl;
    config.Walk(TConfigNode::TraversalOrder::PreOrder, [](const TString& tag, const TConfigNode& node, const int depth) {
        std::cout << std::spacer(4*depth, ' ') << tag << std::endl;
    });

    std::cout << std::endl;
    std::cout << "* InOrder walk:" << std::endl;
    config.Walk(TConfigNode::TraversalOrder::InOrder, [](const TString& tag, const TConfigNode& node, const int depth) {
        std::cout << std::spacer(4*depth, ' ') << tag << std::endl;
    });

    std::cout << std::endl;
    std::cout << "* PostOrder walk:" << std::endl;
    config.Walk(TConfigNode::TraversalOrder::PostOrder, [](const TString& tag, const TConfigNode& node, const int depth) {
        std::cout << std::spacer(4*depth, ' ') << tag << std::endl;
    });

    std::cout << std::endl;
    config.Print();

    std::cout << std::endl;
    config.Dump();
    
    return 0;
}
