cmake_minimum_required(VERSION 3.20)

# Project information
set(PROJECT_TITLE "Configuration File Parser Library")
project(RCParser 
    VERSION 0.1
    DESCRIPTION "Parser library supporting variable data format in ROOT, including download and file caching."
    HOMEPAGE_URL "https://git.ligo.org/kagra/libraries-addons/root/cparser"
)

set(LIBRARY ${PROJECT_NAME})
set(LIBRARY_TITLE ${PROJECT_TITLE})

# Additional cmake features
add_compile_options(-Wextra -Wno-sign-compare -Wimplicit-fallthrough)
set(CMAKE_INSTALL_MESSAGE "LAZY")

# Check if git submodule are initialized in case you use custom cmake modules.
file(GLOB CMAKE_MODULE_PATHS "${CMAKE_CURRENT_SOURCE_DIR}/cmake/*")
foreach(MODULE_PATH ${CMAKE_MODULE_PATHS})
    if(IS_DIRECTORY ${MODULE_PATH})
        list(PREPEND CMAKE_MODULE_PATH ${MODULE_PATH})
    endif()
endforeach()

#
# Prepare project architecture
include(ProjectArchitecture)
include(FindPackageStandard)
include(BuildOptions)
include(DisplayMotd)

DISPLAY_MOTD()
MESSAGE_TITLE("${PROJECT_TITLE}")

update_build_options()
check_build_option(BUILD_IMAGE "Docker container won't be built.")
check_build_option(BUILD_DOCUMENTATION "Library documentation won't be built.")
check_build_option(BUILD_TESTS "Tests for this library won't be running.")
check_build_option(BUILD_LIBRARY "Library `${PROJECT_NAME}` will not be built.")
check_build_option(BUILD_EXAMPLES "Examples how-to-use `${PROJECT_NAME}` won't be computed.")
check_build_option(BUILD_TOOLS "Additional binary tools won't be computed.")

#
# Build project
if(NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/.env AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/cmake/.env.in)
    CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/.env.in ${CMAKE_CURRENT_SOURCE_DIR}/.env @ONLY)
endif()
if(NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/build AND NOT CMAKE_CURRENT_BINARY_DIR STREQUAL ${CMAKE_CURRENT_SOURCE_DIR}/build)
    FILE(CREATE_LINK ${CMAKE_BINARY_DIR_RELATIVE} ${CMAKE_CURRENT_SOURCE_DIR}/build SYMBOLIC)
endif()

#
# Create docker image (used in GitLab CI/CD for running tests)
if(BUILD_IMAGE AND EXISTS Dockerfile)
    include(DockerContainer)
endif()

#
# Additional restriction/warning feature
include(DisableInSourceBuild)
include(GitBranchWarning)

# Project overview
DUMP_PROJECT()

#
# Create a library
if(BUILD_LIBRARY)

    #
    # Load compiler
    include(StandardCompilerC)
    include(StandardCompilerCXX)

    #
    # Library dependencies
    find_package(ROOT REQUIRED COMPONENTS Gui Matrix)
    include(RootFramework)
    find_package(ROOT+ REQUIRED)
    find_package(TOML)
    find_package(INI)
    find_package(XML)
    find_package(JSON)
    find_package(YAML)

    find_package(LALSuite) # @TODO: to be deprecated (to relocate later)
    FILE_SOURCES(SOURCES "src")
    FILE_HEADERS(HEADERS "include" RECURSE EXCLUDE ".*/Impl/.*\.h$" REGEX)
    if(NOT LALSuite_FOUND)
        LIST(FILTER SOURCES EXCLUDE REGEX ".*TLigoFile\.cc$")
        LIST(FILTER HEADERS EXCLUDE REGEX ".*TLigoFile\.h$")
        LIST(FILTER SOURCES EXCLUDE REGEX ".*TLigoTable\.cc$")
        LIST(FILTER HEADERS EXCLUDE REGEX ".*TLigoTable\.h$")
        LIST(FILTER HEADERS EXCLUDE REGEX ".*LALSuite\.h$")
    endif()
    if(NOT TOML_FOUND)
        LIST(FILTER SOURCES EXCLUDE REGEX ".*TConfigToml\.cc$")
        LIST(FILTER HEADERS EXCLUDE REGEX ".*TConfigToml\.h$")
    endif()
    if(NOT JSON_FOUND)
        LIST(FILTER SOURCES EXCLUDE REGEX ".*TConfigJson\.cc$")
        LIST(FILTER HEADERS EXCLUDE REGEX ".*TConfigJson\.h$")
    endif()
    if(NOT XML_FOUND)
        LIST(FILTER SOURCES EXCLUDE REGEX ".*TConfigXml\.cc$")
        LIST(FILTER HEADERS EXCLUDE REGEX ".*TConfigXml\.h$")
    endif()
    if(NOT YAML_FOUND)
        LIST(FILTER SOURCES EXCLUDE REGEX ".*TConfigYaml\.cc$")
        LIST(FILTER HEADERS EXCLUDE REGEX ".*TConfigYaml\.h$")
    endif()
    if(NOT INI_FOUND)
        LIST(FILTER SOURCES EXCLUDE REGEX ".*INIReader\.cc$")
        LIST(FILTER SOURCES EXCLUDE REGEX ".*TConfigIni\.cc$")
        LIST(FILTER HEADERS EXCLUDE REGEX ".*TConfigIni\.h$")
    endif()

    ROOT_ADD_LIBRARY(${LIBRARY}
        SOURCES ${SOURCES}
        HEADERS ${HEADERS}
        PACKAGES PUBLIC ROOT+
        PACKAGES PRIVATE INI XML JSON YAML TOML LALSuite
    )

    ROOT_INSTALL_LIBRARY(${LIBRARY})
    if(${CMAKE_MAINPROJECT})
        
        add_custom_target(lib DEPENDS ${LIBRARY} COMMENT "Compiling library")

        if(BUILD_TESTS AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/tests)
            add_subdirectory("tests")
        endif()

        if(BUILD_EXAMPLES AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/examples)
            add_subdirectory("examples")
        endif()

        if(BUILD_TOOLS AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/tools)
            add_subdirectory("tools")
        endif()
    endif()

endif()

#
# Prepare assets for documentation
if(BUILD_DOCUMENTATION AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/share/doxygen)
    add_subdirectory("share/doxygen")
endif()


#
# Hook Scripts
if(CMAKE_MAINPROJECT)

    # Make sure cmake includes are available
    install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/cmake 
            DESTINATION share 
            PATTERN "*.in" EXCLUDE 
            PATTERN ".*" EXCLUDE
            PATTERN "README*" EXCLUDE
    )

    # Post install message
    find_file(CMAKE_POST_CONFIG PostInstallConfig.cmake PATHS ${CMAKE_MODULE_PATHS})
    if(CMAKE_POST_CONFIG)
        install(SCRIPT "${CMAKE_POST_CONFIG}")
    endif()

    if(CMAKE_BUILD_TYPE STREQUAL "Debug")
        check_target_dependencies()
    endif()

    # Post cmake message
    include(PostCMakeConfig)

endif()