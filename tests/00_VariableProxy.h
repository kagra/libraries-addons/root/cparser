#include <Riostream.h>

#include <TFileReader.h>
#include <TConfig.h>
#include <gtest/gtest.h>

TConfigNode MakeConfig()
{
    TConfigNode myNode;
                myNode["user"]["name"] = "John Doe";
                myNode["user"]["age"] = 30;
                myNode["user"]["address"]["city"] = "Anytown";
                myNode["user"]["address"]["street"] = "123 Main St";
                myNode["user"]["address"]["test"] = nullptr;
                myNode["user"]["array"][0] = "A";
                myNode["user"]["array"][1] = "B";
                myNode["user"]["array"][2] = "C";

    return myNode;
}

TConfigNode MakeConfigIni()
{
    TConfigNode myNode;
                myNode["user"]["name"] = "John Doe";
                myNode["user"]["age"] = 30;

    return myNode;
}
